CREATE DATABASE IF NOT EXISTS sistema_biblioteca;
USE sistema_biblioteca;

CREATE TABLE IF NOT EXISTS Administrador (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS Usuario (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    dni VARCHAR(20) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS UsuarioNoRegistrado (
    id INT AUTO_INCREMENT PRIMARY KEY,
    dni VARCHAR(20) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    telefono VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS Libro (
    id INT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    autor VARCHAR(255) NOT NULL,
    isbn VARCHAR(20) NOT NULL,
    categoria VARCHAR(255) NOT NULL,
    descripcion TEXT NOT NULL,
    cantidad INT NOT NULL,
    cover_image VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS PrestamoFisico (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT,
    id_administrador INT,
    id_libro INT,
    fecha_prestamo DATE NOT NULL,
    fecha_devolucion DATE NOT NULL,
    renovacion BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id),
    FOREIGN KEY (id_administrador) REFERENCES Administrador(id),
    FOREIGN KEY (id_libro) REFERENCES Libro(id)
);

CREATE TABLE PrestamoNoRegistrado (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario_no_registrado INT,
    id_libro INT,
    fecha_prestamo DATE NOT NULL,
    fecha_devolucion DATE NOT NULL,
    renovacion BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (id_usuario_no_registrado) REFERENCES UsuarioNoRegistrado(id),
    FOREIGN KEY (id_libro) REFERENCES Libro(id)
);


CREATE TABLE IF NOT EXISTS Reserva (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT,
    id_libro INT,
    fecha_reserva DATE NOT NULL,
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id),
    FOREIGN KEY (id_libro) REFERENCES Libro(id)
);

CREATE TABLE IF NOT EXISTS HistorialPrestamos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT,
    id_prestamo INT,
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id),
    FOREIGN KEY (id_prestamo) REFERENCES PrestamoFisico(id)
);

CREATE TABLE IF NOT EXISTS Sancion (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT,
    fecha_activacion DATE NOT NULL,
    descripcion TEXT NOT NULL,
    FOREIGN KEY (id_usuario) REFERENCES Usuario(id)
);

ALTER TABLE PrestamoFisico MODIFY fecha_devolucion DATE DEFAULT NULL;

