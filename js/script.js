function showSection(section) {
    document.getElementById('gestion_libros').style.display = 'none';
    document.getElementById('gestion_prestamos').style.display = 'none';
    document.getElementById('gestion_devoluciones').style.display = 'none';
    document.getElementById(section).style.display = 'block';
}

window.onload = function() {
    showSection('gestion_libros');
}

function checkDNI() {
    var dni = document.getElementById('dni').value;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '../public/check_dni.php', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            var response = JSON.parse(this.responseText);
            if (response.exists) {
                document.getElementById('additional_fields').style.display = 'none';
            } else {
                document.getElementById('additional_fields').style.display = 'block';
            }
        }
    };
    xhr.send('dni=' + dni);
}

function searchLibro() {
    var searchTerm = document.getElementById('search_libro').value;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '../public/search_libro.php', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            var libros = JSON.parse(this.responseText);
            var select = document.getElementById('libro_id');
            select.innerHTML = '';
            libros.forEach(function(libro) {
                var option = document.createElement('option');
                option.value = libro.id;
                option.text = libro.titulo + ' - ' + libro.autor;
                select.add(option);
            });
        }
    };
    xhr.send('searchTerm=' + searchTerm);
}
