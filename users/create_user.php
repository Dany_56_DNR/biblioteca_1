<?php
include_once '../config/database.php';

class Admin {
    private $conn;
    private $table_name = "Administrador";

    public $nombre;
    public $apellido;
    public $email;
    public $password;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function create() {
        $query = "INSERT INTO " . $this->table_name . " (nombre, apellido, email, password) VALUES (:nombre, :apellido, :email, :password)";

        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->nombre = htmlspecialchars(strip_tags($this->nombre));
        $this->apellido = htmlspecialchars(strip_tags($this->apellido));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->password = htmlspecialchars(strip_tags($this->password));

        // bind values
        $stmt->bindParam(":nombre", $this->nombre);
        $stmt->bindParam(":apellido", $this->apellido);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":password", $this->password);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }
}

$database = new Database();
$db = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $admin = new Admin($db);

    $admin->nombre = $_POST['nombre'];
    $admin->apellido = $_POST['apellido'];
    $admin->email = $_POST['email'];
    $admin->password = password_hash($_POST['password'], PASSWORD_BCRYPT);

    if ($admin->create()) {
        echo "Administrador creado exitosamente.";
    } else {
        echo "Error al crear el administrador.";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Crear Administrador</title>
</head>
<body>
    <h1>Crear Administrador</h1>
    <form method="POST" action="create_user.php">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" id="nombre" required><br>
        <label for="apellido">Apellido:</label>
        <input type="text" name="apellido" id="apellido" required><br>
        <label for="email">Email:</label>
        <input type="email" name="email" id="email" required><br>
        <label for="password">Contraseña:</label>
        <input type="password" name="password" id="password" required><br>
        <button type="submit">Crear Administrador</button>
    </form>
</body>
</html>
