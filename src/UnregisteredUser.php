<?php
class UnregisteredUser {
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    public function create($data) {
        $sql = "INSERT INTO UsuarioNoRegistrado (dni, nombre, apellido, telefono) VALUES (?, ?, ?, ?)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($data);
    }

    public function find($dni) {
        $sql = "SELECT * FROM UsuarioNoRegistrado WHERE dni = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$dni]);
        return $stmt->fetch();
    }
}
?>
