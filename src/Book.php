<?php
class Book {
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    public function all() {
        $sql = "SELECT * FROM Libro";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function find($id) {
        $sql = "SELECT * FROM Libro WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$id]);
        return $stmt->fetch();
    }

    public function create($data) {
        $sql = "INSERT INTO Libro (titulo, autor, isbn, categoria, descripcion, cantidad, cover_image) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($data);
    }

    public function update($id, $data) {
        $sql = "UPDATE Libro SET titulo = ?, autor = ?, isbn = ?, categoria = ?, descripcion = ?, cantidad = ?, cover_image = ? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(array_merge($data, [$id]));
    }

    public function delete($id) {
        $sql = "DELETE FROM Libro WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$id]);
    }
}
?>
