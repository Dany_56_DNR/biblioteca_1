<?php
class Loan {
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    public function create($data) {
        $sql = "INSERT INTO PrestamoFisico (id_usuario, id_administrador, id_libro, fecha_prestamo, fecha_devolucion, renovacion) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($data);
    }

    public function update($id, $data) {
        $sql = "UPDATE PrestamoFisico SET fecha_devolucion = ?, renovacion = ? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(array_merge($data, [$id]));
    }

    public function delete($id) {
        $sql = "DELETE FROM PrestamoFisico WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$id]);
    }

    public function findByUser($user_id) {
        $sql = "SELECT * FROM PrestamoFisico WHERE id_usuario = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$user_id]);
        return $stmt->fetchAll();
    }
}
?>
