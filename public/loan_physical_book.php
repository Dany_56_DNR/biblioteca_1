<?php
require '../config/database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $dni = $_POST['dni'];
    $libro_id = $_POST['libro_id'];
    $admin_id = $_POST['admin_id'];

    $sql = "SELECT * FROM Usuario WHERE dni = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$dni]);
    $user = $stmt->fetch();

    if ($user) {
        $user_id = $user['id'];
    } else {
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $email = $_POST['email'];

        $sql = "INSERT INTO Usuario (nombre, apellido, email, dni) VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$nombre, $apellido, $email, $dni]);

        $user_id = $pdo->lastInsertId();
    }

    $sql = "UPDATE Libro SET cantidad = cantidad - 1 WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$libro_id]);

    $fecha_prestamo = date('Y-m-d');
    $fecha_devolucion = date('Y-m-d', strtotime('+4 days'));
    $sql = "INSERT INTO PrestamoFisico (id_usuario, id_administrador, id_libro, fecha_prestamo, fecha_devolucion, renovacion) VALUES (?, ?, ?, ?, ?, 0)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$user_id, $admin_id, $libro_id, $fecha_prestamo, $fecha_devolucion]);

    header("Location: ../dashboards/bibliotecario_dashboard.php");
}
?>
