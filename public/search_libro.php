<?php
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $searchTerm = $_POST['searchTerm'];
    
    $sql = "SELECT id, titulo, autor FROM Libro WHERE isbn LIKE ? OR titulo LIKE ? OR autor LIKE ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['%' . $searchTerm . '%', '%' . $searchTerm . '%', '%' . $searchTerm . '%']);
    $libros = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($libros);
}
?>
