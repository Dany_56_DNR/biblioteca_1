<?php
session_start();
require '../config/database.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $libro_id = $_POST['libro_id'];
    $user_id = $_SESSION['user_id'];

    $sql = "SELECT cantidad FROM Libro WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$libro_id]);
    $libro = $stmt->fetch();

    if ($libro['cantidad'] > 0) {
        $sql = "UPDATE Libro SET cantidad = cantidad - 1 WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$libro_id]);

        $fecha_prestamo = date('Y-m-d');
        $fecha_devolucion = date('Y-m-d', strtotime('+4 days'));
        $sql = "INSERT INTO PrestamoFisico (id_usuario, id_libro, fecha_prestamo, fecha_devolucion) VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$user_id, $libro_id, $fecha_prestamo, $fecha_devolucion]);

        header("Location: ../dashboards/user_dashboard.php");
    } else {
        echo "El libro no está disponible.";
    }
}
?>
