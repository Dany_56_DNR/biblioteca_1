<?php
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM Usuario WHERE email = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$email]);
    $user = $stmt->fetch();

    if ($user && password_verify($password, $user['password'])) {
        session_start();
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['user_name'] = $user['nombre'];
        header("Location: ../dashboards/user_dashboard.php");
    } else {
        $sql = "SELECT * FROM Administrador WHERE email = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$email]);
        $admin = $stmt->fetch();

        if ($admin && password_verify($password, $admin['password'])) {
            session_start();
            $_SESSION['user_id'] = $admin['id'];
            $_SESSION['user_name'] = $admin['nombre'];
            header("Location: ../dashboards/bibliotecario_dashboard.php");
        } else {
            echo "Email o contraseña incorrectos";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" href="../styles/auth_styles.css">
</head>
<body>
    <div class="container">
        <h1>Iniciar sesión</h1>
        
        <form method="POST" action="login.php">
            <input type="email" name="email" id="email" placeholder="Correo electrónico" required>
            <input type="password" name="password" id="password" placeholder="Contraseña" required>
            <button type="submit">Iniciar sesión</button>
        </form>
        <div class="footer">
            ¿No tienes una cuenta? <a href="register_user.php">Regístrate gratis</a><br>
            
        </div>
    </div>
</body>
</html>
