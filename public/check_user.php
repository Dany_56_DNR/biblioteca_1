<?php
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $dni = $_POST['dni'];
    
    $sql = "SELECT * FROM Usuario WHERE dni = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$dni]);
    $usuario = $stmt->fetch();

    echo json_encode(['exists' => $usuario ? true : false]);
}
?>
