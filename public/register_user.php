<?php
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $email = $_POST['email'];
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $dni = $_POST['dni'];

    $sql = "INSERT INTO Usuario (nombre, apellido, email, password, dni) VALUES (?, ?, ?, ?, ?)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$nombre, $apellido, $email, $password, $dni]);

    header("Location: login.php");
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Registrar Usuario</title>
    <link rel="stylesheet" href="../styles/auth_styles.css">
</head>
<body>
    <div class="container">
        <h1>Crear tu cuenta</h1>
        
        <form method="POST" action="register_user.php">
            <input type="text" name="nombre" id="nombre" placeholder="Nombre" required>
            <input type="text" name="apellido" id="apellido" placeholder="Apellido" required>
            <input type="email" name="email" id="email" placeholder="Correo electrónico" required>
            <input type="password" name="password" id="password" placeholder="Contraseña" required>
            <input type="text" name="dni" id="dni" placeholder="DNI" required>
            <button type="submit">Registrar</button>
        </form>
        <div class="footer">
            ¿Ya tienes cuenta? <a href="login.php">Inicia sesión</a>
        </div>
    </div>
</body>
</html>
