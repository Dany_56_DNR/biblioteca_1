<?php
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

$sql = "SELECT * FROM Libro";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$libros = $stmt->fetchAll();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Biblioteca Central</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/index.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap">
   
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="index.php">Biblioteca Central</a>
                </li>
                <li>
                    <a href="login.php">Iniciar Sesión</a>
                    <a href="register_user.php">Registrarse</a>
                </li>
            </ul>
        </nav>
    </header>

    <video autoplay muted loop class="video-background">
        <source src="../img/fondoindex.mp4" type="video/mp4">
        Tu navegador no soporta la etiqueta de video.
    </video>

    <main>
        <div class="search-container">
            <input type="text" id="search" placeholder="¿Qué quieres leer hoy ?">
            <button type="button"><i class="fas fa-search"></i></button>
        </div>
        <div class="book-container">
            <?php foreach ($libros as $libro): ?>
                <div class="book">
                    <div class="cover">
                        <img src="../uploads/book_covers/<?php echo htmlspecialchars($libro['cover_image']); ?>" alt="Portada de <?php echo htmlspecialchars($libro['titulo']); ?>" style="width:100%; height:100%;">
                    </div>
                    <div class="details">
                        <p><strong><?php echo htmlspecialchars($libro['titulo']); ?></strong></p>
                        <p><?php echo htmlspecialchars($libro['autor']); ?></p>
                        <p><?php echo htmlspecialchars($libro['descripcion']); ?></p>
                        <p><?php echo htmlspecialchars($libro['cantidad'] <= 0 ? 'Agotado' : $libro['cantidad']); ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </main>

    <footer>
        <p>© 2024 Biblioteca Central. Todos los derechos reservados.</p>
    </footer>

    <script>
        document.querySelector('button').addEventListener('click', function() {
            var searchValue = document.getElementById('search').value.toLowerCase();
            var books = document.querySelectorAll('.book');

            books.forEach(function(book) {
                var title = book.querySelector('.details p strong').textContent.toLowerCase();
                if (title.includes(searchValue)) {
                    book.style.display = 'block';
                } else {
                    book.style.display = 'none';
                }
            });
        });
    </script>
</body>
</html>
