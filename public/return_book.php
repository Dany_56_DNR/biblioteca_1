<?php
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $prestamo_id = $_POST['prestamo_id'];

    // Verificar si el préstamo es de un usuario registrado
    $sql = "SELECT id_libro, id_usuario FROM PrestamoFisico WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$prestamo_id]);
    $prestamo = $stmt->fetch();

    if ($prestamo) {
        // Devolución para préstamo de usuario registrado
        $libro_id = $prestamo['id_libro'];
        $user_id = $prestamo['id_usuario'];

        // Actualizar la fecha de devolución a la fecha actual
        $sql = "UPDATE PrestamoFisico SET fecha_devolucion = NOW() WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$prestamo_id]);

        // Actualizar la cantidad del libro
        $sql = "UPDATE Libro SET cantidad = cantidad + 1 WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$libro_id]);

        header("Location: ../dashboards/bibliotecario_dashboard.php?message=Devolución registrada exitosamente");
        exit();
    } else {
        // Verificar si el préstamo es de un usuario no registrado
        $sql = "SELECT id_libro, id_usuario_no_registrado FROM PrestamoNoRegistrado WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$prestamo_id]);
        $prestamo = $stmt->fetch();

        if ($prestamo) {
            // Devolución para préstamo de usuario no registrado
            $libro_id = $prestamo['id_libro'];
            $user_id_no_registrado = $prestamo['id_usuario_no_registrado'];

            // Eliminar el préstamo de la tabla PrestamoNoRegistrado
            $sql = "DELETE FROM PrestamoNoRegistrado WHERE id = ?";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$prestamo_id]);

            // Actualizar la cantidad del libro
            $sql = "UPDATE Libro SET cantidad = cantidad + 1 WHERE id = ?";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$libro_id]);

            header("Location: ../dashboards/bibliotecario_dashboard.php?message=Devolución registrada exitosamente");
            exit();
        } else {
            header("Location: ../dashboards/bibliotecario_dashboard.php?message=Préstamo no encontrado");
            exit();
        }
    }
}
?>
