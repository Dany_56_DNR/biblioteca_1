<?php
require '../config/database.php';
require('../public/fpdf186/fpdf.php'); // Asegúrate de que la ruta a fpdf.php sea correcta

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $prestamo_id = $_POST['prestamo_id'];

    // Verificar si el préstamo es de un usuario registrado
    $sql = "SELECT id_libro, id_usuario, fecha_devolucion, renovacion FROM PrestamoFisico WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$prestamo_id]);
    $prestamo = $stmt->fetch();

    if ($prestamo) {
        // Verificar si el préstamo ya ha sido extendido
        if ($prestamo['renovacion']) {
            header("Location: ../dashboards/bibliotecario_dashboard.php?message=El préstamo ya ha sido extendido anteriormente");
            exit();
        }

        // Extensión para préstamo de usuario registrado
        $nueva_fecha_devolucion = date('Y-m-d', strtotime($prestamo['fecha_devolucion']. ' + 7 days')); // Extender por 7 días

        $sql = "UPDATE PrestamoFisico SET fecha_devolucion = ?, renovacion = TRUE WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$nueva_fecha_devolucion, $prestamo_id]);

        header("Location: ../dashboards/bibliotecario_dashboard.php?message=Préstamo extendido exitosamente");
        exit();
    } else {
        // Verificar si el préstamo es de un usuario no registrado
        $sql = "SELECT id_libro, id_usuario_no_registrado, fecha_devolucion, renovacion FROM PrestamoNoRegistrado WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$prestamo_id]);
        $prestamo = $stmt->fetch();

        if ($prestamo) {
            // Verificar si el préstamo ya ha sido extendido
            if ($prestamo['renovacion']) {
                header("Location: ../dashboards/bibliotecario_dashboard.php?message=El préstamo ya ha sido extendido anteriormente");
                exit();
            }

            // Extensión para préstamo de usuario no registrado
            $nueva_fecha_devolucion = date('Y-m-d', strtotime($prestamo['fecha_devolucion']. ' + 7 days')); // Extender por 7 días

            $sql = "UPDATE PrestamoNoRegistrado SET fecha_devolucion = ?, renovacion = TRUE WHERE id = ?";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$nueva_fecha_devolucion, $prestamo_id]);

            // Obtener detalles del usuario no registrado
            $sql = "SELECT nombre, apellido FROM UsuarioNoRegistrado WHERE id = ?";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$prestamo['id_usuario_no_registrado']]);
            $usuario_no_registrado = $stmt->fetch();

            // Obtener detalles del libro
            $sql = "SELECT titulo FROM Libro WHERE id = ?";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$prestamo['id_libro']]);
            $libro = $stmt->fetch();

            // Generar boleta de extensión en PDF
            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->Cell(0, 10, 'Boleta de Extension de Prestamo', 0, 1, 'C');
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(0, 10, 'Nombre: ' . $usuario_no_registrado['nombre'], 0, 1);
            $pdf->Cell(0, 10, 'Apellido: ' . $usuario_no_registrado['apellido'], 0, 1);
            $pdf->Cell(0, 10, 'Libro: ' . $libro['titulo'], 0, 1);
            $pdf->Cell(0, 10, 'Fecha de Prestamo Original: ' . $prestamo['fecha_devolucion'], 0, 1);
            $pdf->Cell(0, 10, 'Nueva Fecha de Devolucion: ' . $nueva_fecha_devolucion, 0, 1);
            $pdf->Output();

            exit();
        } else {
            header("Location: ../dashboards/bibliotecario_dashboard.php?message=Préstamo no encontrado");
            exit();
        }
    }
}
?>
