<?php
require '../config/database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $titulo = $_POST['titulo'];
    $autor = $_POST['autor'];
    $isbn = $_POST['isbn'];
    $categoria = $_POST['categoria'];
    $descripcion = $_POST['descripcion'];
    $cantidad = $_POST['cantidad'];

    $cover_image = '';
    if (!empty($_FILES['cover_image']['name'])) {
        $cover_image = basename($_FILES['cover_image']['name']);
        move_uploaded_file($_FILES['cover_image']['tmp_name'], '../uploads/book_covers/' . $cover_image);
    }

    $sql = "INSERT INTO Libro (titulo, autor, isbn, categoria, descripcion, cantidad, cover_image) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$titulo, $autor, $isbn, $categoria, $descripcion, $cantidad, $cover_image]);

    header("Location: ../dashboards/bibliotecario_dashboard.php");
}
?>

<form method="POST" enctype="multipart/form-data">
    <input type="text" name="titulo" placeholder="Título" required>
    <input type="text" name="autor" placeholder="Autor" required>
    <input type="text" name="isbn" placeholder="ISBN" required>
    <input type="text" name="categoria" placeholder="Categoría" required>
    <textarea name="descripcion" placeholder="Descripción" required></textarea>
    <input type="number" name="cantidad" placeholder="Cantidad" required>
    <input type="file" name="cover_image">
    <button type="submit">Registrar Libro</button>
</form>
