<?php
require('../fpdf/fpdf.php');
require('../config/database.php');

$database = new Database();
$pdo = $database->getConnection();

if (isset($_GET['usuario_id']) && isset($_GET['libro_id'])) {
    $usuario_id = $_GET['usuario_id'];
    $libro_id = $_GET['libro_id'];

    // Obtener los datos del usuario no registrado
    $sql = "SELECT * FROM UsuarioNoRegistrado WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$usuario_id]);
    $usuario = $stmt->fetch();

    // Obtener los datos del libro
    $sql = "SELECT * FROM Libro WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$libro_id]);
    $libro = $stmt->fetch();

    // Obtener la fecha de préstamo y devolución
    $sql = "SELECT * FROM PrestamoNoRegistrado WHERE id_usuario_no_registrado = ? AND id_libro = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$usuario_id, $libro_id]);
    $prestamo = $stmt->fetch();

    // Crear el PDF de la boleta
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 16);
    $pdf->Cell(0, 10, 'Boleta de Préstamo', 0, 1, 'C');
    $pdf->Ln(10);

    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(0, 10, 'Nombre: ' . $usuario['nombre'] . ' ' . $usuario['apellido'], 0, 1);
    $pdf->Cell(0, 10, 'DNI: ' . $usuario['dni'], 0, 1);
    $pdf->Cell(0, 10, 'Teléfono: ' . $usuario['telefono'], 0, 1);
    $pdf->Ln(10);

    $pdf->Cell(0, 10, 'Título del Libro: ' . $libro['titulo'], 0, 1);
    $pdf->Cell(0, 10, 'Autor: ' . $libro['autor'], 0, 1);
    $pdf->Cell(0, 10, 'ISBN: ' . $libro['isbn'], 0, 1);
    $pdf->Ln(10);

    $pdf->Cell(0, 10, 'Fecha de Préstamo: ' . $prestamo['fecha_prestamo'], 0, 1);
    $pdf->Cell(0, 10, 'Fecha de Devolución: ' . $prestamo['fecha_devolucion'], 0, 1);

    $pdf->Output();
} else {
    echo "Datos insuficientes para generar la boleta.";
}
?>
