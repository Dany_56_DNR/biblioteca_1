<?php
require '../../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if (isset($_GET['id'])) {
    $libro_id = $_GET['id'];

    $sql = "SELECT * FROM Libro WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$libro_id]);
    $libro = $stmt->fetch();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $titulo = $_POST['titulo'];
    $autor = $_POST['autor'];
    $isbn = $_POST['isbn'];
    $categoria = $_POST['categoria'];
    $descripcion = $_POST['descripcion'];
    $cantidad = $_POST['cantidad'];
    $libro_id = $_POST['libro_id'];

    $cover_image = $libro['cover_image'];
    if (!empty($_FILES['cover_image']['name'])) {
        $cover_image = basename($_FILES['cover_image']['name']);
        $target_dir = '../../uploads/book_covers/';
        $target_path = $target_dir . $cover_image;

        // Crear el directorio si no existe
        if (!is_dir($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        if (move_uploaded_file($_FILES['cover_image']['tmp_name'], $target_path)) {
            // Archivo subido exitosamente
        } else {
            echo "Error al subir el archivo.";
            exit();
        }
    }

    $sql = "UPDATE Libro SET titulo = ?, autor = ?, isbn = ?, categoria = ?, descripcion = ?, cantidad = ?, cover_image = ? WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$titulo, $autor, $isbn, $categoria, $descripcion, $cantidad, $cover_image, $libro_id]);

    header("Location: ../../dashboards/bibliotecario_dashboard.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Editar Libro</title>
    <link rel="stylesheet" href="../../styles/dashboard_styles.css">
</head>
<body>
    

    <div class="main-content">
        <div class="header">
            <h1>Editar Libro</h1>
        </div>

        <div class="card">
            <form method="POST" enctype="multipart/form-data">
                <input type="hidden" name="libro_id" value="<?php echo htmlspecialchars($libro['id']); ?>">
                <label for="titulo">Título:</label>
                <input type="text" name="titulo" id="titulo" value="<?php echo htmlspecialchars($libro['titulo']); ?>" required><br>
                <label for="autor">Autor:</label>
                <input type="text" name="autor" id="autor" value="<?php echo htmlspecialchars($libro['autor']); ?>" required><br>
                <label for="isbn">ISBN:</label>
                <input type="text" name="isbn" id="isbn" value="<?php echo htmlspecialchars($libro['isbn']); ?>" required><br>
                <label for="categoria">Categoría:</label>
                <input type="text" name="categoria" id="categoria" value="<?php echo htmlspecialchars($libro['categoria']); ?>" required><br>
                <label for="descripcion">Descripción:</label>
                <textarea name="descripcion" id="descripcion" required><?php echo htmlspecialchars($libro['descripcion']); ?></textarea><br>
                <label for="cantidad">Cantidad:</label>
                <input type="number" name="cantidad" id="cantidad" value="<?php echo htmlspecialchars($libro['cantidad']); ?>" required><br>
                <label for="cover_image">Portada:</label>
                <input type="file" name="cover_image" id="cover_image"><br>
                <button type="submit" class="btn">Actualizar Libro</button>
                <a href="../../dashboards/bibliotecario_dashboard.php" class="btn btn-logout">Regresar</a>
            </form>
        </div>
    </div>
</body>
</html>
