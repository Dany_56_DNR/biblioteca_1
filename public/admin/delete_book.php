<?php
require '../../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if (isset($_GET['id'])) {
    $libro_id = $_GET['id'];

    $sql = "DELETE FROM Libro WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$libro_id]);

    header("Location: ../../dashboards/bibliotecario_dashboard.php");
    exit();
}
?>
