<?php
session_start();
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $libro_id = $_POST['libro_id'];
    $user_id = $_SESSION['user_id'];

    try {
        $pdo->beginTransaction();

        $sql = "INSERT INTO Reserva (id_usuario, id_libro, fecha_reserva) VALUES (?, ?, NOW())";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$user_id, $libro_id]);

        $sql = "UPDATE Libro SET cantidad = cantidad - 1 WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$libro_id]);

        $pdo->commit();

        header("Location: ../user/user_dashboard.php?message=Libro reservado exitosamente");
        exit();
    } catch (Exception $e) {
        $pdo->rollBack();
        echo "Error al reservar el libro: " . $e->getMessage();
    }
}
?>
