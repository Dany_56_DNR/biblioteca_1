<?php
require '../config/database.php';
require('../public/fpdf186/fpdf.php'); // Asegúrate de que la ruta a fpdf.php sea correcta

$database = new Database();
$pdo = $database->getConnection();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $libro_id = $_POST['libro_id'];
    $dni = $_POST['dni'];
    $admin_id = $_POST['admin_id'];

    // Verificar si el libro está disponible
    $sql = "SELECT cantidad FROM Libro WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$libro_id]);
    $libro = $stmt->fetch();

    if ($libro['cantidad'] <= 0) {
        header("Location: ../dashboards/bibliotecario_dashboard.php?message=El libro está agotado");
        exit();
    }

    // Verificar si el usuario está registrado
    $sql = "SELECT id FROM Usuario WHERE dni = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$dni]);
    $usuario = $stmt->fetch();

    if ($usuario) {
        $user_id = $usuario['id'];

        // Verificar si el usuario ya tiene 3 préstamos activos
        $sql = "SELECT COUNT(*) as total FROM PrestamoFisico WHERE id_usuario = ? AND fecha_devolucion >= NOW()";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$user_id]);
        $count = $stmt->fetch();

        if ($count['total'] >= 3) {
            header("Location: ../dashboards/bibliotecario_dashboard.php?message=El usuario ya tiene 3 préstamos activos");
            exit();
        }

        // Verificar si el usuario ya tiene un préstamo activo del mismo libro
        $sql = "SELECT COUNT(*) as total FROM PrestamoFisico WHERE id_usuario = ? AND id_libro = ? AND fecha_devolucion >= NOW()";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$user_id, $libro_id]);
        $count = $stmt->fetch();

        if ($count['total'] > 0) {
            header("Location: ../dashboards/bibliotecario_dashboard.php?message=El usuario ya tiene un préstamo activo de este libro");
            exit();
        }

        // Registrar el préstamo
        $fecha_prestamo = date('Y-m-d');
        $fecha_devolucion = date('Y-m-d', strtotime($fecha_prestamo . ' + 7 days')); // 7 días de préstamo

        $sql = "INSERT INTO PrestamoFisico (id_usuario, id_administrador, id_libro, fecha_prestamo, fecha_devolucion) VALUES (?, ?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$user_id, $admin_id, $libro_id, $fecha_prestamo, $fecha_devolucion]);

        // Actualizar la cantidad del libro
        $sql = "UPDATE Libro SET cantidad = cantidad - 1 WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$libro_id]);

        header("Location: ../dashboards/bibliotecario_dashboard.php?message=Préstamo registrado exitosamente");
        exit();
    } else {
        // Manejo para usuario no registrado
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $telefono = $_POST['telefono'];

        // Verificar si el usuario no registrado ya tiene 3 préstamos activos
        $sql = "SELECT COUNT(*) as total FROM PrestamoNoRegistrado WHERE id_usuario_no_registrado IN (SELECT id FROM UsuarioNoRegistrado WHERE dni = ?) AND fecha_devolucion >= NOW()";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$dni]);
        $count = $stmt->fetch();

        if ($count['total'] >= 3) {
            header("Location: ../dashboards/bibliotecario_dashboard.php?message=El usuario no registrado ya tiene 3 préstamos activos");
            exit();
        }

        // Verificar si el usuario no registrado ya tiene un préstamo activo del mismo libro
        $sql = "SELECT COUNT(*) as total FROM PrestamoNoRegistrado WHERE id_usuario_no_registrado IN (SELECT id FROM UsuarioNoRegistrado WHERE dni = ?) AND id_libro = ? AND fecha_devolucion >= NOW()";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$dni, $libro_id]);
        $count = $stmt->fetch();

        if ($count['total'] > 0) {
            header("Location: ../dashboards/bibliotecario_dashboard.php?message=El usuario no registrado ya tiene un préstamo activo de este libro");
            exit();
        }

        // Registrar al usuario no registrado si no existe
        $sql = "SELECT id FROM UsuarioNoRegistrado WHERE dni = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$dni]);
        $usuario_no_registrado = $stmt->fetch();

        if (!$usuario_no_registrado) {
            $sql = "INSERT INTO UsuarioNoRegistrado (dni, nombre, apellido, telefono) VALUES (?, ?, ?, ?)";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$dni, $nombre, $apellido, $telefono]);
            $user_id_no_registrado = $pdo->lastInsertId();
        } else {
            $user_id_no_registrado = $usuario_no_registrado['id'];
        }

        // Registrar el préstamo
        $fecha_prestamo = date('Y-m-d');
        $fecha_devolucion = date('Y-m-d', strtotime($fecha_prestamo . ' + 7 days')); // 7 días de préstamo

        $sql = "INSERT INTO PrestamoNoRegistrado (id_usuario_no_registrado, id_libro, fecha_prestamo, fecha_devolucion) VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$user_id_no_registrado, $libro_id, $fecha_prestamo, $fecha_devolucion]);

        // Actualizar la cantidad del libro
        $sql = "UPDATE Libro SET cantidad = cantidad - 1 WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$libro_id]);

        // Generar boleta de préstamo en PDF
        $sql = "SELECT titulo FROM Libro WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$libro_id]);
        $libro = $stmt->fetch();

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(0, 10, 'Boleta de Prestamo', 0, 1, 'C');
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0, 10, 'Nombre: ' . $nombre, 0, 1);
        $pdf->Cell(0, 10, 'Apellido: ' . $apellido, 0, 1);
        $pdf->Cell(0, 10, 'DNI: ' . $dni, 0, 1);
        $pdf->Cell(0, 10, 'Libro: ' . $libro['titulo'], 0, 1);
        $pdf->Cell(0, 10, 'Fecha de Prestamo: ' . $fecha_prestamo, 0, 1);
        $pdf->Cell(0, 10, 'Fecha de Devolucion: ' . $fecha_devolucion, 0, 1);
        $pdf->Output();

        exit();
    }
}
?>
