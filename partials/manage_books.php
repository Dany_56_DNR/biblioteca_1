<?php
require '../config/database.php';

$sql = "SELECT * FROM Libro";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$libros = $stmt->fetchAll();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Gestión de Libros</title>
</head>
<body>
    <h1>Gestión de Libros</h1>
    <a href="../public/admin/add_book.php">Agregar Libro</a>
    <ul>
        <?php foreach ($libros as $libro): ?>
            <li><?php echo htmlspecialchars($libro['titulo']); ?> - <?php echo htmlspecialchars($libro['autor']); ?>
                <a href="../public/admin/edit_book.php?id=<?php echo $libro['id']; ?>">Editar</a>
                <a href="../public/admin/delete_book.php?id=<?php echo $libro['id']; ?>">Eliminar</a>
            </li>
        <?php endforeach; ?>
    </ul>
</body>
</html>
