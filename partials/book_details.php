<?php
require '../config/database.php';

if (isset($_GET['id'])) {
    $libro_id = $_GET['id'];

    $sql = "SELECT * FROM Libro WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$libro_id]);
    $libro = $stmt->fetch();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Detalles del Libro</title>
</head>
<body>
    <h1><?php echo htmlspecialchars($libro['titulo']); ?></h1>
    <p>Autor: <?php echo htmlspecialchars($libro['autor']); ?></p>
    <p>ISBN: <?php echo htmlspecialchars($libro['isbn']); ?></p>
    <p>Categoría: <?php echo htmlspecialchars($libro['categoria']); ?></p>
    <p>Descripción: <?php echo htmlspecialchars($libro['descripcion']); ?></p>
    <p>Cantidad: <?php echo htmlspecialchars($libro['cantidad']); ?></p>
    <img src="../public/uploads/book_covers/<?php echo htmlspecialchars($libro['cover_image']); ?>" alt="Portada del libro">
</body>
</html>
