<?php
require '../config/database.php';

$sql = "SELECT * FROM PrestamoFisico";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$prestamos = $stmt->fetchAll();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Préstamos</title>
</head>
<body>
    <h1>Préstamos Activos</h1>
    <ul>
        <?php foreach ($prestamos as $prestamo): ?>
            <li>ID del Usuario: <?php echo htmlspecialchars($prestamo['id_usuario']); ?>, ID del Libro: <?php echo htmlspecialchars($prestamo['id_libro']); ?>, Fecha de Devolución: <?php echo htmlspecialchars($prestamo['fecha_devolucion']); ?></li>
        <?php endforeach; ?>
    </ul>
</body>
</html>
