<?php
session_start();
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../public/login.php");
    exit();
}

$user_id = $_SESSION['user_id'];
$sql = "SELECT * FROM Administrador WHERE id = ?";
$stmt = $pdo->prepare($sql);
$stmt->execute([$user_id]);
$admin = $stmt->fetch();

if (!$admin) {
    header("Location: ../public/login.php");
    exit();
}

$message = isset($_GET['message']) ? htmlspecialchars($_GET['message']) : '';

// Obtener préstamos activos y filtrar por ISBN si se busca
$prestamos = [];
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['isbn_devolucion'])) {
    $isbn_devolucion = $_POST['isbn_devolucion'];
    $sql = "SELECT p.id, p.id_libro, l.titulo, u.nombre, u.apellido, p.fecha_prestamo, p.fecha_devolucion 
            FROM PrestamoFisico p 
            JOIN Libro l ON p.id_libro = l.id 
            JOIN Usuario u ON p.id_usuario = u.id 
            WHERE l.isbn = ? AND p.fecha_devolucion >= NOW()
            UNION
            SELECT p.id, p.id_libro, l.titulo, un.nombre, un.apellido, p.fecha_prestamo, p.fecha_devolucion 
            FROM PrestamoNoRegistrado p 
            JOIN Libro l ON p.id_libro = l.id 
            JOIN UsuarioNoRegistrado un ON p.id_usuario_no_registrado = un.id 
            WHERE l.isbn = ? AND p.fecha_devolucion >= NOW()";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$isbn_devolucion, $isbn_devolucion]);
    $prestamos = $stmt->fetchAll();
} else {
    $sql = "SELECT p.id, p.id_libro, l.titulo, u.nombre, u.apellido, p.fecha_prestamo, p.fecha_devolucion 
            FROM PrestamoFisico p 
            JOIN Libro l ON p.id_libro = l.id 
            JOIN Usuario u ON p.id_usuario = u.id 
            WHERE p.fecha_devolucion >= NOW()
            UNION
            SELECT p.id, p.id_libro, l.titulo, un.nombre, un.apellido, p.fecha_prestamo, p.fecha_devolucion 
            FROM PrestamoNoRegistrado p 
            JOIN Libro l ON p.id_libro = l.id 
            JOIN UsuarioNoRegistrado un ON p.id_usuario_no_registrado = un.id 
            WHERE p.fecha_devolucion >= NOW()";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $prestamos = $stmt->fetchAll();
}
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Dashboard del Bibliotecario</title>
    <link rel="stylesheet" href="../styles/dashboard_styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="../js/script.js"></script>
    <style>
        .cards { display: none; }
        .active { display: block; }
    </style>
    <script>
        function showSection(sectionId) {
            var sections = document.querySelectorAll('.cards');
            sections.forEach(function(section) {
                section.classList.remove('active');
            });
            document.getElementById(sectionId).classList.add('active');
        }

        document.addEventListener("DOMContentLoaded", function() {
            var defaultSection = "<?php echo isset($_POST['isbn_devolucion']) ? 'gestion_devoluciones' : 'gestion_libros'; ?>";
            showSection(defaultSection);
        });
    </script>
</head>
<body>
    <div class="sidebar">
        <div class="profile">
            <img src="../img/use.png" alt="Profile Picture">
            <h2><?php echo htmlspecialchars($admin['nombre']); ?></h2>
        </div>
        <nav>
            <ul>
                <li><a href="#" onclick="showSection('gestion_libros')"><i class="fas fa-book"></i> Gestión de Libros</a></li>
                <li><a href="#" onclick="showSection('gestion_prestamos')"><i class="fas fa-hand-holding"></i> Gestión de Préstamos</a></li>
                <li><a href="#" onclick="showSection('gestion_devoluciones')"><i class="fas fa-undo"></i> Gestión de Devoluciones</a></li>
            </ul>
        </nav>
        <a href="../public/logout.php" class="btn btn-logout"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a>
    </div>

    <div class="main-content">
        <div class="header">
            <h1>Dashboard del Bibliotecario</h1>
            <?php if ($message): ?>
                <div class="message">
                    <?php echo $message; ?>
                </div>
            <?php endif; ?>
        </div>

        <section id="gestion_libros" class="cards active">
            <div class="card">
                <h2>Gestión de Libros</h2>
                <a href="../public/admin/add_book.php" class="btn">Agregar Libro</a>
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Autor</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM Libro";
                        $stmt = $pdo->prepare($sql);
                        $stmt->execute();
                        $libros = $stmt->fetchAll();
                        foreach ($libros as $libro): ?>
                            <tr>
                                <td><?php echo htmlspecialchars($libro['titulo']); ?></td>
                                <td><?php echo htmlspecialchars($libro['autor']); ?></td>
                                <td><?php echo htmlspecialchars($libro['cantidad'] <= 0 ? 'Agotado' : $libro['cantidad']); ?></td>
                                <td>
                                    <a href="../public/admin/edit_book.php?id=<?php echo $libro['id']; ?>" class="btn btn-icon"><i class="fas fa-edit"></i></a>
                                    <a href="../public/admin/delete_book.php?id=<?php echo $libro['id']; ?>" class="btn btn-icon"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </section>

        <section id="gestion_prestamos" class="cards">
            <div class="card">
                <h2>Gestión de Préstamos</h2>
                <form method="POST" action="../partials/add_loan.php">
                    <label for="search_libro">Buscar Libro:</label>
                    <input type="text" id="search_libro" onkeyup="searchLibro()"><br>
                    <label for="libro_id">Seleccionar Libro:</label>
                    <select name="libro_id" id="libro_id" required></select><br>
                    <label for="dni">DNI del Usuario:</label>
                    <input type="text" name="dni" id="dni" required onblur="checkDNI()"><br>
                    <div id="additional_fields" style="display:none;">
                        <label for="nombre">Nombre:</label>
                        <input type="text" name="nombre" id="nombre"><br>
                        <label for="apellido">Apellido:</label>
                        <input type="text" name="apellido" id="apellido"><br>
                        <label for="telefono">Teléfono:</label>
                        <input type="text" name="telefono" id="telefono"><br>
                    </div>
                    <input type="hidden" name="admin_id" value="<?php echo $admin['id']; ?>">
                    <button type="submit" class="btn">Prestar Libro</button>
                </form>
            </div>
        </section>

        <section id="gestion_devoluciones" class="cards">
            <div class="card">
                <h2>Gestión de Devoluciones</h2>
                <form method="POST" action="bibliotecario_dashboard.php">
                    <label for="isbn_devolucion">Buscar por ISBN:</label>
                    <input type="text" name="isbn_devolucion" id="isbn_devolucion" required>
                    <button type="submit" class="btn">Buscar</button>
                </form>
                <?php
                if ($prestamos) {
                    echo "<h3>Libros Prestados</h3>";
                    echo "<ul>";
                    foreach ($prestamos as $prestamo) {
                        echo "<li>
                                Libro: " . htmlspecialchars($prestamo['titulo']) . " - 
                                Usuario: " . htmlspecialchars($prestamo['nombre']) . " " . htmlspecialchars($prestamo['apellido']) . " - 
                                Fecha de préstamo: " . htmlspecialchars($prestamo['fecha_prestamo']) . " - 
                                Fecha de devolución: " . htmlspecialchars($prestamo['fecha_devolucion']) . "
                                <form method='POST' action='../public/return_book.php'>
                                    <input type='hidden' name='prestamo_id' value='" . $prestamo['id'] . "'>
                                    <button type='submit' class='btn'>Registrar Devolución</button>
                                </form>
                                <form method='POST' action='../public/extend_loan.php'>
                                    <input type='hidden' name='prestamo_id' value='" . $prestamo['id'] . "'>
                                    <button type='submit' class='btn'>Extender Préstamo</button>
                                </form>
                              </li>";
                    }
                    echo "</ul>";
                } else {
                    echo "No se encontraron préstamos activos.";
                }
                ?>
            </div>
        </section>
    </div>
</body>
</html>
