<?php
session_start();
require '../config/database.php';

$database = new Database();
$pdo = $database->getConnection();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../public/login.php");
    exit();
}

$user_id = $_SESSION['user_id'];
$user_name = $_SESSION['user_name'];

// Obtener préstamos activos
$sql = "SELECT p.id, l.titulo, p.fecha_prestamo, p.fecha_devolucion 
        FROM PrestamoFisico p 
        JOIN Libro l ON p.id_libro = l.id 
        WHERE p.id_usuario = ? AND p.fecha_devolucion >= NOW()";
$stmt = $pdo->prepare($sql);
$stmt->execute([$user_id]);
$prestamos_activos = $stmt->fetchAll();

// Obtener historial de préstamos
$sql = "SELECT p.id, l.titulo, p.fecha_prestamo, p.fecha_devolucion 
        FROM PrestamoFisico p 
        JOIN Libro l ON p.id_libro = l.id 
        WHERE p.id_usuario = ? AND p.fecha_devolucion < NOW()";
$stmt = $pdo->prepare($sql);
$stmt->execute([$user_id]);
$historial_prestamos = $stmt->fetchAll();

// Obtener todos los libros
$sql = "SELECT * FROM Libro";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$libros = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Dashboard del Usuario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap">
    <link rel="stylesheet" href="../styles/stylesS.css">
    <style>
        /* Estilos generales */
        body {
            font-family: 'Roboto', sans-serif; /* Cambia 'Roboto' si usas otra fuente */
            margin: 0;
            padding: 0;
            background: url('../img/FONDO.jpg') no-repeat center center fixed;
            background-size: cover;
            color: #f4f4f4;
        }

        a {
            text-decoration: none;
            color: inherit;
        }

        ul {
            list-style-type: none;
            padding: 0;
        }

        header {
            background-color: rgba(0, 0, 0, 0.9); /* Cambié a 0.9 para hacer el fondo menos transparente */
            color: #fff;
            padding: 20px;
            text-align: center;
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            z-index: 1000;
        }

        header nav ul {
            display: flex;
            justify-content: center;
            gap: 20px;
        }

        header nav ul li {
            padding: 10px;
        }

        header nav ul li a {
            color: #fff;
            font-weight: bold;
            transition: color 0.3s ease;
        }

        header nav ul li a:hover {
            color: #d4edda;
        }

        main {
            padding-top: 80px;
        }

        .content {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #1e1e1e;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        h1 {
            text-align: center;
            color: #007bff;
        }

        h2 {
            color: #007bff;
            margin-bottom: 20px;
        }

        ul {
            list-style-type: none;
            padding: 0;
        }

        .loan-item, .history-item {
            background-color: #282828;
            margin: 10px 0;
            padding: 15px;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            color: #fff;
        }

        .search-container {
            display: flex;
            justify-content: center;
            margin-bottom: 20px;
            padding: 20px;
        }

        .search-container input {
            width: 300px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-family: 'Roboto', sans-serif; /* Asegúrate de aplicar la fuente también aquí */
        }

        .search-container button {
            padding: 10px;
            border: none;
            border-radius: 5px;
            background-color: #007bff;
            color: #fff;
            cursor: pointer;
            margin-left: 10px;
        }

        .search-container button:hover {
            background-color: #0056b3;
        }

        .book-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            gap: 20px;
            padding: 20px;
        }

        .book {
            position: relative;
            border-radius: 10px;
            width: 200px;
            height: 300px;
            background-color: whitesmoke;
            box-shadow: 1px 1px 12px #000;
            transform: preserve-3d;
            perspective: 2000px;
            display: flex;
            align-items: center;
            justify-content: center;
            color: #000;
        }

        .cover {
            top: 0;
            position: absolute;
            background-color: lightgray;
            width: 100%;
            height: 100%;
            border-radius: 10px;
            cursor: pointer;
            transition: all 0.5s;
            transform-origin: 0;
            box-shadow: 1px 1px 12px #000;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .book:hover .cover {
            transition: all 0.5s;
            transform: rotateY(-80deg);
        }

        .details {
            display: none;
            position: absolute;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            padding: 20px;
            text-align: center;
        }

        .book:hover .details {
            display: block;
            transform: rotateY(0);
        }

        footer {
            background-color: #007bff;
            color: #fff;
            text-align: center;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            width: 100%;
        }

        @media (max-width: 600px) {
            .book {
                width: 150px;
                height: 200px;
            }

            .search-container input {
                width: 200px;
            }
        }

        @media (max-width: 900px) {
            .book {
                width: 180px;
                height: 240px;
            }

            .search-container input {
                width: 250px;
            }
        }

    </style>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.php"><i class="fas fa-home"></i> Inicio</a></li>
                <li><a href="user_dashboard.php"><i class="fas fa-user"></i> Mi Cuenta</a></li>
                <li><a href="../public/logout.php"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a></li>
            </ul>
        </nav>
    </header>

    <main>
        <section class="content">
            <h1>Bienvenido, <?php echo htmlspecialchars($user_name); ?></h1>
            <div class="loan-section">
                <h2>Mis Préstamos Activos</h2>
                <?php if ($prestamos_activos): ?>
                    <ul>
                        <?php foreach ($prestamos_activos as $prestamo): ?>
                            <li class="loan-item">
                                <strong>Libro:</strong> <?php echo htmlspecialchars($prestamo['titulo']); ?> <br>
                                <strong>Fecha de Préstamo:</strong> <?php echo htmlspecialchars($prestamo['fecha_prestamo']); ?> <br>
                                <strong>Fecha de Devolución:</strong> <?php echo htmlspecialchars($prestamo['fecha_devolucion']); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    <p>No tienes préstamos activos.</p>
                <?php endif; ?>
            </div>

            <div class="history-section">
                <h2>Historial de Préstamos</h2>
                <?php if ($historial_prestamos): ?>
                    <ul>
                        <?php foreach ($historial_prestamos as $prestamo): ?>
                            <li class="history-item">
                                <strong>Libro:</strong> <?php echo htmlspecialchars($prestamo['titulo']); ?> <br>
                                <strong>Fecha de Préstamo:</strong> <?php echo htmlspecialchars($prestamo['fecha_prestamo']); ?> <br>
                                <strong>Fecha de Devolución:</strong> <?php echo htmlspecialchars($prestamo['fecha_devolucion']); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    <p>No tienes préstamos anteriores.</p>
                <?php endif; ?>
            </div>

            <div class="search-section">
                <h2>Buscar Libros</h2>
                <div class="search-container">
                    <input type="text" id="search" placeholder="¿Qué quieres leer hoy?">
                    <button type="button"><i class="fas fa-search"></i></button>
                </div>
                <div class="book-container">
                    <?php foreach ($libros as $libro): ?>
                        <div class="book">
                            <div class="cover">
                                <img src="../uploads/book_covers/<?php echo htmlspecialchars($libro['cover_image']); ?>" alt="Portada de <?php echo htmlspecialchars($libro['titulo']); ?>" style="width:100%; height:100%;">
                            </div>
                            <div class="details">
                                <p><strong><?php echo htmlspecialchars($libro['titulo']); ?></strong></p>
                                <p><?php echo htmlspecialchars($libro['autor']); ?></p>
                                <p><?php echo htmlspecialchars($libro['descripcion']); ?></p>
                                <p><?php echo htmlspecialchars($libro['cantidad'] <= 0 ? 'Agotado' : $libro['cantidad']); ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    </main>

    <footer>
        <p>© 2024 Biblioteca. Todos los derechos reservados.</p>
    </footer>

    <script>
        document.querySelector('button').addEventListener('click', function() {
            var searchValue = document.getElementById('search').value.toLowerCase();
            var books = document.querySelectorAll('.book');

            books.forEach(function(book) {
                var title = book.querySelector('.details p strong').textContent.toLowerCase();
                if (title.includes(searchValue)) {
                    book.style.display = 'block';
                } else {
                    book.style.display = 'none';
                }
            });
        });
    </script>
</body>
</html>
